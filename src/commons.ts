export const languageMap = new Map([
    ["plantuml", "plantuml"],
    ["dot", "graphviz"],
    ["er", "erd"]
]); 

export const RENDER_SERVICE = "https://kroki.io";

export enum OutputFormat { svg = "svg" , png = "png" };