import { NotebookCell, NotebookCellKind, NotebookCellOutput, NotebookCellOutputItem, NotebookController, NotebookDocument, notebooks } from "vscode";
import { languageMap } from "./commons";
import { toSVG } from "./figure-converter";

export class ArchitectureNotebookController {
  readonly controllerId = 'test-notebook-renderer-id';
  readonly notebookType = 'test-notebook-renderer';
  readonly label = 'My Notebook';
  readonly supportedLanguages = [...languageMap.keys()];

  private readonly _controller: NotebookController;
  private _executionOrder = 0;

  constructor() {
    this._controller = notebooks.createNotebookController(
      this.controllerId,
      this.notebookType,
      this.label
    );

    this._controller.supportedLanguages = this.supportedLanguages;
    this._controller.supportsExecutionOrder = true;
    this._controller.executeHandler = this._execute.bind(this);
  }

  private _execute(
    cells: NotebookCell[],
    _notebook: NotebookDocument,
    _controller: NotebookController
  ): void {
    for (let cell of cells) {
      this._doExecution(cell);
    }
  }

  private async _doExecution(cell: NotebookCell): Promise<void> {
    const execution = this._controller.createNotebookCellExecution(cell);
    execution.executionOrder = ++this._executionOrder;
    execution.start(Date.now()); // Keep track of elapsed time to execute cell.

    /* Do some execution here; not implemented */
    if (cell.kind === NotebookCellKind.Code) {
      toSVG(cell.document.languageId, cell.document.getText()).then(svgOut => {
        execution.replaceOutput([
          new NotebookCellOutput([
            NotebookCellOutputItem.text(svgOut, "image/svg+xml")
          ])
        ]);
        execution.end(true, Date.now());
      }).catch((error) => { 
        execution.replaceOutput([
          new NotebookCellOutput([
            NotebookCellOutputItem.error(error)
          ])
        ]);
        execution.end(false, Date.now());
      });
    }
  }

  dispose(): void {
    this._controller.dispose();
  }
}