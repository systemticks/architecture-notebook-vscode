import { CancellationToken, NotebookCellData, NotebookCellKind, NotebookData, NotebookSerializer } from "vscode";
import { TextDecoder, TextEncoder } from "util";

interface RawNotebookData {
    cells: RawNotebookCell[]
  }
  
interface RawNotebookCell {
    language: string;
    value: string;
    kind: NotebookCellKind;
    editable?: boolean;
}

export class ArchitectureNotebookSerializer implements NotebookSerializer {

    deserializeNotebook(content: Uint8Array, token: CancellationToken): NotebookData | Thenable<NotebookData> {

        var contents = new TextDecoder().decode(content);    // convert to String to make JSON object

        // Read file contents
        let raw: RawNotebookData;
        try {
          raw = <RawNotebookData>JSON.parse(contents);
        } catch {
          raw = { cells: [] };
        }
    
        // Create array of Notebook cells for the VS Code API from file contents
        const cells = raw.cells.map(item => new NotebookCellData(
          item.kind,
          item.value,
          item.language
        ));
    
        // Pass read and formatted Notebook Data to VS Code to display Notebook with saved cells
        return new NotebookData(
          cells
        );
    }

    serializeNotebook(data: NotebookData, token: CancellationToken): Uint8Array | Thenable<Uint8Array> {
        let contents: RawNotebookData = { cells: []};

        for (const cell of data.cells) {
          contents.cells.push({
            kind: cell.kind,
            language: cell.languageId,
            value: cell.value
          });
        }
    
        // Give a string of all the data to save and VS Code will handle the rest
        return new TextEncoder().encode(JSON.stringify(contents));
    }
    
}