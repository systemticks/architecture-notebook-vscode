import got from 'got';
import * as pako from 'pako';
import urljoin = require('url-join');

import { languageMap, RENDER_SERVICE, OutputFormat } from './commons';

export async function toSVG(languageId: string, content: string): Promise<string> {

    const _type = languageMap.get(languageId);

    if(_type) {
        try {
            const encodedContent = encode(content);
            if(encodedContent) {
                const response = await got(urljoin(RENDER_SERVICE, _type, OutputFormat.svg, encodedContent));
                return response.body;        
            }
            else {
                return Promise.reject(new Error("Encoding Error"));
            }
        }
        catch (error:  any) {
            return Promise.reject(error);
        }
    }

    return Promise.reject(new Error("Language Not supported"));
}

function encode(content: string): string | null {

    try {
        const data = Buffer.from(content, 'utf8');
        const compressed = pako.deflate(data, { level: 9 });
        return Buffer.from(compressed)
            .toString('base64')
            .replace(/\+/g, '-').replace(/\//g, '_');
    } catch (err) {
        console.error(err);
    }

    return null;
}
